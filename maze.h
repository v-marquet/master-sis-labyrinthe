#ifndef MAZE_H
#define MAZE_H


#define bool int
#define true  1
#define false 0

typedef struct _maze {
    int size_x;   // number of columns (width)
    int size_y;   // number of lines (height)
    int start_x;  // x coordinate of start position
    int start_y;  // y coordinate of start position
    int exit_x;   // x coordinate of exit position
    int exit_y;   // y coordinaet of exit position
    int pos_x;    // x coordinate of the person in the maze
    int pos_y;    // y coordinate of the person in the maze
    unsigned short int** data;   // maze geometry data + optional additional data
    // data[x][y] = cell of coordinate (x,y) = cell at column x, line y
} maze;


#define TOP_WALL_MASK    32768  // 10000000 00000000, 2^15
#define RIGHT_WALL_MASK  16384  // 01000000 00000000, 2^14
#define BOTTOM_WALL_MASK 8192   // 00100000 00000000, 2^13
#define LEFT_WALL_MASK   4096   // 00010000 00000000, 2^12

#define WALL_CHANCE 4  // (must be integer) the higher the value, the less walls in the maze


maze* instantiate_maze(int size_x, int size_y, int start_x, int start_y, int exit_x, int exit_y);
void create_random_geometry(maze* maze);
void display_maze(maze* maze);
maze* load_maze_from_file(const char* filename);

bool is_wall_top(maze* maze);
bool is_wall_right(maze* maze);
bool is_wall_bottom(maze* maze);
bool is_wall_left(maze* maze);

#endif
