#ifndef AI_H
#define AI_H


// Theses masks serves two purposes:
// 1. indicate that a cell has already been visited
// 2. indicate what is the path to go back
#define PATH_TOP_MASK    2048  // 2^11, 00001000 00000000
#define PATH_RIGHT_MASK  1024  // 2^10, 00000100 00000000
#define PATH_BOTTOM_MASK 512   // 2^9,  00000010 00000000
#define PATH_LEFT_MASK   256   // 2^8,  00000001 00000000


bool check_if_exit_reached(maze* maze);
bool make_one_move(maze* maze);
bool is_visited(maze* maze, int x, int y);  // necessary for the display


#endif
