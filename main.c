#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "maze.h"
#include "AI.h"


void usage(const char* exe_name);
void solve_maze(maze* maze, int wait);

int main(int argc, char const *argv[])
{
    int i = 1;
    int filename_index = -1; // the index of the file to load in argv
                       // (-1 if no file to load)
    int wait = 1000;  // number of milliseconds between two moves in the maze

    while(i < argc) {
        if (strcmp(argv[i], "--load") == 0 || strcmp(argv[i], "-l") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "ERROR: --load: precise file to load. Aborting.\n");
                usage(argv[0]);
                exit(1);
            }
            filename_index = i;
        }
        else if (strcmp(argv[i], "--speed") == 0 || strcmp(argv[i], "-s") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "ERROR: precise speed after --speed. Aborting.\n");
                usage(argv[0]);
                exit(1);
            }
            wait = atoi(argv[i]);
            if (wait <= 10 || wait >= 10000)
                wait = 1000;
        }
        else if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0) {
            usage(argv[0]);
        }
        else {
            fprintf(stderr, "ERROR: argument(s) not known. Ignoring it.\n");
            usage(argv[0]);
        }

        i++;
    }

    if (filename_index == -1) {  // no input file, create random maze
        maze* maze = instantiate_maze(30, 10, 0, 0, 5, 5);
        create_random_geometry(maze);
        solve_maze(maze, wait);
        free(maze);
    }
    else {  // load maze from file
        maze* maze = load_maze_from_file(argv[filename_index]);
        solve_maze(maze, wait);
        free(maze);
    }

    return 0;
}


void solve_maze(maze* maze, int wait) {
    while(true) {
        display_maze(maze);
        if (check_if_exit_reached(maze)) {
        	printf("Exit reached !\n");
        	break;
        }
        if (!make_one_move(maze)) {
        	printf("Exit not found.\n");
            break;
        }
        usleep(wait * 1000);
    }
}


void usage(const char* exe_name) {
    printf("Usage: %s [--load FILE] [--speed SPEED]\n\n", exe_name);
    printf("Parameters:\n");
    printf("  --load or -l : path to the maze file to load\n");
    printf("  --speed or -h : number of milliseconds between two moves in the maze\n");
}
