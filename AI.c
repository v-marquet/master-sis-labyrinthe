#include <stdlib.h>
#include <stdio.h>
#include "maze.h"
#include "AI.h"


/*
 * Functions to set the path back (see comments in AI.h)
 * Example: set_path_back_top(maze) means that you arrived
 *  at current cell (determined by character position) from it's top cell
 */
void set_path_back_top(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    maze->data[x][y] = maze->data[x][y] | PATH_TOP_MASK;
}

void set_path_back_right(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    maze->data[x][y] = maze->data[x][y] | PATH_RIGHT_MASK;
}

void set_path_back_bottom(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    maze->data[x][y] = maze->data[x][y] | PATH_BOTTOM_MASK;
}

void set_path_back_left(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    maze->data[x][y] = maze->data[x][y] | PATH_LEFT_MASK;
}

/*
 * Functions to get the path back for a cell
 */
bool is_path_back_top(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return (maze->data[x][y] & PATH_TOP_MASK) >> 11;
}

bool is_path_back_right(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return (maze->data[x][y] & PATH_RIGHT_MASK) >> 10;
}

bool is_path_back_bottom(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return (maze->data[x][y] & PATH_BOTTOM_MASK) >> 9;
}

bool is_path_back_left(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return (maze->data[x][y] & PATH_LEFT_MASK) >> 8;
}


/*
 * Function to get if the cell of coordinates (x,y) has been already visited
 */
bool is_visited(maze* maze, int x, int y) {
    if (x == maze->start_x && y == maze->start_y) // start position is already visited
        return true;
    else if ( ((maze->data[x][y] & PATH_TOP_MASK) >> 11) == 1
        || ((maze->data[x][y] & PATH_RIGHT_MASK)  >> 10) == 1
        || ((maze->data[x][y] & PATH_BOTTOM_MASK) >> 9)  == 1
        || ((maze->data[x][y] & PATH_LEFT_MASK)   >> 8)  == 1)
        return true;
    else
        return false;
}

/*
 * Functions to get if a relative cell of the cell of coordinate (x,y)
 *  has been already visited
 */
bool is_top_visited(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return is_visited(maze, x, y-1);
}

bool is_right_visited(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return is_visited(maze, x+1, y);
}

bool is_bottom_visited(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return is_visited(maze, x, y+1);
}

bool is_left_visited(maze* maze) {
    int x = maze->pos_x, y = maze->pos_y;
    return is_visited(maze, x-1, y);
}


/*
 * Check if the character has found the exit
 */
bool check_if_exit_reached(maze* maze) {
    if (maze->pos_x == maze->exit_x && maze->pos_y == maze->exit_y)
        return true;
    else
        return false;
}


/*
 * Make the character make one move inside the maze
 * Return true if one move was made, false otherwise
 *  (will return false if all maze has already been visited)
 */
bool make_one_move(maze* maze) {
    int x = maze->pos_x;
    int y = maze->pos_y;

    // First, we try to move to a cell that has not already been visited
    // Order of moves: left, top, right, bottom
    if (!is_wall_left(maze) && !is_left_visited(maze)) {
        move_character_left(maze);
        set_path_back_right(maze);
        return true;
    }
    else if (!is_wall_top(maze) && !is_top_visited(maze)) {
        move_character_top(maze);
        set_path_back_bottom(maze);
        return true;
    }
    else if (!is_wall_right(maze) && !is_right_visited(maze)) {
        move_character_right(maze);
        set_path_back_left(maze);
        return true;
    }
    else if (!is_wall_bottom(maze) && !is_bottom_visited(maze)) {
        move_character_bottom(maze);
        set_path_back_top(maze);
        return true;
    }

    // If all next cells have already been visited, we go back
    // to the cell from which we're coming
    if (is_path_back_left(maze)) {
        move_character_left(maze);
        return true;
    }
    else if (is_path_back_top(maze)) {
        move_character_top(maze);
        return true;
    }
    else if (is_path_back_right(maze)) {
        move_character_right(maze);
        return true;
    }
    else if (is_path_back_bottom(maze)) {
        move_character_bottom(maze);
        return true;
    }

    // If there is no path back, it means we reached our starting point
    //  and the exit is not accessible (too much walls)
    return false;
}
