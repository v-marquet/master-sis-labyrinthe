Labyrinthe 2D
=============
Projet Master SIS 2015.


Utilisation
-----------
* compilation: `make`
* execution: `./maze`
* to see program option, use option `-h`: `./maze -h`


Code organisation
-----------------
* `main.c`: functions to parse parameters, and launch the maze simulation depending on the parameters
* `maze.c`: functions to deal with the maze structure (create maze, display maze, ...)
* `AI.c`: the artificial intelligence of the character in the maze
