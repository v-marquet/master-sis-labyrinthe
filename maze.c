#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "maze.h"
#include "AI.h"

/*
 * Instantiate a maze and set the metadata
 *  WARNING: the "data" field of the maze struct is left uninitialized
 */
maze* instantiate_maze(int size_x, int size_y, int start_x, int start_y, int exit_x, int exit_y) {
    maze* m = malloc(sizeof(maze));
    if (m == NULL) {
        fprintf(stderr, "ERROR: Can't allocate memory for maze struct.\n");
        exit(1);
    }

    m->size_x = size_x;
    m->size_y = size_y;

    m->start_x = start_x;
    m->start_y = start_y;

    m->pos_x = start_x;
    m->pos_y = start_y;

    m->exit_x = exit_x;
    m->exit_y = exit_y;

    m->data = malloc(sizeof(unsigned short int*)*size_x);
    if (m->data == NULL) {
        fprintf(stderr, "ERROR: Can't allocate memory for maze struct.\n");
        exit(1);
    }

    int i, j;
    for(i=0; i<size_x; i++) {
        m->data[i] = malloc(sizeof(unsigned short int)*size_y);
        if (m->data[i] == NULL) {
            fprintf(stderr, "ERROR: Can't allocate memory for maze struct.\n");
            exit(1);
        }

        // set all memory to 0 (= no wall)
        for(j=0; j<size_y; j++)
            m->data[i][j] = 0;
    }

    return m;
}

/*
 * Create a random maze geometry ("data" field of the maze struct)
 *  from a maze struct whose headers MUST have been initialized
 */
void create_random_geometry(maze* maze) {
    srand(getpid());  // not very random but we don't care
    int i, j;

    // we create maze, from left to right and from top to bottom
    for(j=0; j<maze->size_y; j++) {
        for(i=0; i<maze->size_x; i++) {
            // we put walls all around the maze to prevent from escaping it
            if (j == 0)
                maze->data[i][j] |= TOP_WALL_MASK;
            if (i == maze->size_x-1)
                maze->data[i][j] |= RIGHT_WALL_MASK;
            if (j == maze->size_y-1)
                maze->data[i][j] |= BOTTOM_WALL_MASK;
            if (i == 0)
                maze->data[i][j] |= LEFT_WALL_MASK;

            // we add random bottom and right walls, with probability 1/4
            if ((rand() % WALL_CHANCE) == 0)
                maze->data[i][j] |= BOTTOM_WALL_MASK;
            if ((rand() % WALL_CHANCE) == 0)
                maze->data[i][j] |= RIGHT_WALL_MASK;

            // the top wall existence of current cell depends on the bottom of upper cell
            if (j > 0) {
                int top_cell_bottom_wall = (maze->data[i][j-1] & BOTTOM_WALL_MASK) >> 13; // result: 0 or 1
                if (top_cell_bottom_wall == 1)  // a wall at bottom of upper cell
                    maze->data[i][j] |= TOP_WALL_MASK;  // set top of current cell to a wall
            }

            // the left wall existence of current cell depends on the right of the left cell
            if (i > 0) {
                int left_cell_right_wall = (maze->data[i-1][j] & RIGHT_WALL_MASK) >> 14;
                if (left_cell_right_wall == 1)  // no wall at right of left cell
                    maze->data[i][j] |= LEFT_WALL_MASK;
            }
        }
    }
}

/*
 * Return true if there is a wall at top of cell, false otherwise
 */
bool is_wall_top_at(maze* maze, int x, int y) {
    return (maze->data[x][y] & TOP_WALL_MASK) >> 15;
}

bool is_wall_top(maze* maze) {
    return is_wall_top_at(maze, maze->pos_x, maze->pos_y);
}

/*
 * Return true if there is a wall at right of cell, false otherwise
 */
bool is_wall_right_at(maze* maze, int x, int y) {
    return (maze->data[x][y] & RIGHT_WALL_MASK) >> 14;
}

bool is_wall_right(maze* maze) {
    return is_wall_right_at(maze, maze->pos_x, maze->pos_y);
}

/*
 * Return true if there is a wall at bottom of cell, false otherwise
 */
bool is_wall_bottom_at(maze* maze, int x, int y) {
    return (maze->data[x][y] & BOTTOM_WALL_MASK) >> 13;
}

bool is_wall_bottom(maze* maze) {
    return is_wall_bottom_at(maze, maze->pos_x, maze->pos_y);
}

/*
 * Return true if there is a wall at left of cell, false otherwise
 */
bool is_wall_left_at(maze* maze, int x, int y) {
    return (maze->data[x][y] & LEFT_WALL_MASK) >> 12;
}

bool is_wall_left(maze* maze) {
    return is_wall_left_at(maze, maze->pos_x, maze->pos_y);
}

/*
 * Display cell symbols (☺ or ⚐ or ⚑ or SPACE)
 */
void display_cell(maze* maze, int x, int y) {
	if (x == maze->pos_x && y == maze->pos_y)
		printf("\e[93m☺\e[0m");
	else if (x == maze->exit_x && y == maze->exit_y)
		printf("\e[32m⚑\e[0m");
	else if (x == maze->start_x && y == maze->start_y)
		printf("\e[31m⚑\e[0m");
    else if (is_visited(maze, x, y))
        printf("\e[2m*\e[0m");
	else
		printf(" ");
}

/*
 * Display a maze in the terminal
 */
void display_maze(maze* maze) {
    int i, j;

    // the line 0
    printf("┌─");
    for (i=1; i<maze->size_x; i++)
        is_wall_left_at(maze, i, 0) ? printf("┬─") : printf("──");
    printf("┐\n");

    printf("│");
    display_cell(maze, 0, 0);
    for (i=1; i<maze->size_x; i++) {
        is_wall_left_at(maze, i, 0) ? printf("│") : printf(" ");
        display_cell(maze, i, 0);
    }
    printf("│\n");

    // lines for i > 0
    for (j=1; j<maze->size_y; j++) {
        // left cell
        i = 0;
        is_wall_top_at(maze, i, j) ? printf("├─") : printf("│ ");

        for (i=1; i<maze->size_x; i++) {
            if (is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("┼─");
            else if (!is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("├─");
            else if (is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("┤ ");
            else if (is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("┬─");
            else if (is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("┴─");
            else if (!is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("│ ");
            else if (!is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("┌─");
            else if (!is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("└─");
            else if (is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("┐ ");
            else if (is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("┘ ");
            else if (is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("──");
            else if (!is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && is_wall_left_at(maze, i, j))
                printf("╷ ");
            else if (is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("╴ ");
            else if (!is_wall_top_at(maze, i-1, j) && is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("╶─");
            else if (!is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("╵ ");
            else if (!is_wall_top_at(maze, i-1, j) && !is_wall_top_at(maze, i, j) && !is_wall_left_at(maze, i, j-1) && !is_wall_left_at(maze, i, j))
                printf("  ");
        }
        is_wall_top_at(maze, maze->size_x-1, j) ? printf("┤\n") : printf("│\n");

        printf("│");
        display_cell(maze, 0, j);
        for (i=1; i<maze->size_x; i++) {
            is_wall_left_at(maze, i, j) ? printf("│") : printf(" ");
            display_cell(maze, i, j);
        }
        printf("│\n");
    }
    
    printf("└─");
    for (i=1; i<maze->size_x; i++)
        is_wall_left_at(maze, i, maze->size_y-1) ? printf("┴─") : printf("──");
    printf("┘\n");
}

/*
 * Load a maze from a file
 * Return a NULL pointer if the file to load is not valid
 */
maze* load_maze_from_file(const char* filename) {
    FILE* f_in = fopen(filename, "r");
    if (f_in == NULL) {
        fprintf(stderr, "ERROR: can't open file %s. Aborting.\n", filename);
        exit(1);
    }

    int size_x, size_y, start_x, start_y, exit_x, exit_y;
    int result, i, j;

    result = fscanf(f_in, "%d", &size_x);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the number of lines of maze in the file. Aborting.\n");
        exit(1);
    }
    if (size_x <= 0) {
        fprintf(stderr, "ERROR: number of lines of maze should be >0 (currently is %d). Aborting.\n", size_x);
        exit(1);
    }

    result = fscanf(f_in, "%d", &size_y);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the number of columns of maze in the file. Aborting.\n");
        exit(1);
    }
    if (size_y <= 0) {
        fprintf(stderr, "ERROR: number of columns of maze should be >0 (currently is %d). Aborting.\n", size_y);
        exit(1);
    }

    printf("INFO: maze width = %d and maze height = %d\n", size_x, size_y);

    result = fscanf(f_in, "%d", &start_x);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the x coordinate of start position of maze in the file. Aborting.\n");
        exit(1);
    }
    if (start_x < 0 || start_x > size_x-1) {
        fprintf(stderr, "ERROR: x coordinate of start position should be inside maze. Aborting.\n");
        exit(1);
    }

    result = fscanf(f_in, "%d", &start_y);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the y coordinate of start position of maze in the file. Aborting.\n");
        exit(1);
    }
    if (start_y < 0 || start_y > size_y-1) {
        fprintf(stderr, "ERROR: y coordinate of start position should be inside maze. Aborting.\n");
        exit(1);
    }

    printf("INFO: start position x coordinate = %d and start position y coordinate = %d\n", start_x, start_y);

    result = fscanf(f_in, "%d", &exit_x);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the x coordinate of exit position of maze in the file. Aborting.\n");
        exit(1);
    }
    if (exit_x < 0 || exit_x > size_x-1) {
        fprintf(stderr, "ERROR: x coordinate of exit position should be inside maze. Aborting.\n");
        exit(1);
    }

    result = fscanf(f_in, "%d", &exit_y);
    if (result == 0 || result == EOF) {
        fprintf(stderr, "ERROR: can't read the y coordinate of exit position of maze in the file. Aborting.\n");
        exit(1);
    }
    if (exit_y < 0 || exit_y > size_y-1) {
        fprintf(stderr, "ERROR: y coordinate of exit position should be inside maze. Aborting.\n");
        exit(1);
    }

    printf("INFO: exit position x coordinate = %d and exit position y coordinate = %d\n", exit_x, exit_y);

    maze* maze = instantiate_maze(size_x, size_y, start_x, start_y, exit_x, exit_y);

    for (j=0; j<size_y; j++) {
        for (i=0; i<size_x; i++) {
            result = fscanf(f_in, "%hu", &maze->data[i][j]);  // reminder: hu = half unsigned (int)
            if (result == 0 || result == EOF) {
                fprintf(stderr, "ERROR: can't get walls for position x=%d y=%d. Aborting.\n", i, j);
                exit(1);
            }

            // make sure that we get only set the bits for the walls, and that the others are 0
            maze->data[i][j] = maze->data[i][j] & 61440;  // 61440 = 11110000 00000000

            // check that there are walls all around the maze (so we can't escape)
            if (i == 0) {
                if (!is_wall_left_at(maze, i, j)) {
                    fprintf(stderr, "ERROR: left cells should have a wall at the left. Aborting.\n");
                    exit(1);
                }
            }
            if (i == size_x-1) {
                if (!is_wall_right_at(maze, i, j)) {
                    fprintf(stderr, "ERROR: right cells should have a wall at the right. Aborting.\n");
                    exit(1);
                }
            }
            if (j == 0) {
                if (!is_wall_top_at(maze, i, j)) {
                    fprintf(stderr, "ERROR: top cells should have a wall at the top. Aborting.\n");
                    exit(1);
                }
            }
            if (j == size_y-1) {
                if (!is_wall_bottom_at(maze, i, j)) {
                    fprintf(stderr, "ERROR: bottom cells should have a wall at the bottom. Aborting.\n");
                    exit(1);
                }
            }

            // check wall consistency
            if (j > 0) {
                if (is_wall_top_at(maze, i, j) != is_wall_bottom_at(maze, i, j-1)) {
                    fprintf(stderr, "ERROR: top wall of cell %d:%d and bottom wall of cell", i, j);
                    fprintf(stderr, "%d:%d are not consistent. Aborting.\n", i, j-1);
                    exit(1);
                }
            }
            if (i > 0) {
                if (is_wall_left_at(maze, i, j) != is_wall_right_at(maze, i-1, j)) {
                    fprintf(stderr, "ERROR: left wall of cell %d:%d and right wall of cell", i, j);
                    fprintf(stderr, "%d:%d are not consistent. Aborting.\n", i-1, j);
                    exit(1);
                }
            }
        }
    }

    return maze;
}


/*
 * Functions to move the character in the maze
 */
void move_character_top(maze* maze) {
    maze->pos_y -= 1;
}

void move_character_right(maze* maze) {
    maze->pos_x += 1;
}

void move_character_bottom(maze* maze) {
    maze->pos_y += 1;
}

void move_character_left(maze* maze) {
    maze->pos_x -= 1;
}
