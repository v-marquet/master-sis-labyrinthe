# C compiler to use
CC = gcc

# compilation flags
CFLAGS = -Wall -g

# target executable name
TARGET = maze


all: maze

maze: main.o maze.o AI.o
	$(CC) -o $(TARGET) main.o maze.o AI.o

main.o: main.c
	$(CC) -o main.o -c main.c

maze.o: maze.c
	$(CC) -o maze.o -c maze.c

AI.o: AI.c
	$(CC) -o AI.o -c AI.c

clean:
	rm -rf *.o $(TARGET)
